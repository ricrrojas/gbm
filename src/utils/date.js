import moment from "moment";

export const formatDatetime = datetime =>
  moment(datetime).format("YYYY-MM-DD hh:mm:ss");

export const formatTime = time => moment(time).format("hh:mm:ss");
