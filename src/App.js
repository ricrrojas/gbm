import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { SnackbarProvider } from "material-ui-snackbar-provider";
import PrivateRoute from "./components/PrivateRoute";
import IPCVierwer from "./screens/IPCViewer";
import Login from "./screens/Home";
import Register from "./screens/Registro";

import Header from "./components/Header";
import styles from "./App.module.css";
import api from "./api";

class App extends React.Component {
  state = {
    isAuthenticated: false
  };

  onLogin = ({ user }, callback) => {
    localStorage.setItem("user", JSON.stringify(user));
    this.setState(
      {
        isAuthenticated: true,
        user
      },
      callback
    );
  };

  _logout = () => {
    localStorage.removeItem("user");
    this.setState(state => {
      state.isAuthenticated = false;
      delete state.user;
      return state;
    }, api.logout);
  };

  render() {
    const { isAuthenticated, redirectToReferrer, user } = this.state;

    return (
      <SnackbarProvider SnackbarProps={{ autoHideDuration: 4000 }}>
        <div className={styles.container}>
          <Header
            isAuthenticated={isAuthenticated}
            user={user}
            logout={this._logout}
          />
          <BrowserRouter className={styles.routeWrapper}>
            <React.Fragment>
              <Route
                exact
                path="/"
                render={routeProps => (
                  <Login
                    {...routeProps}
                    isAuthenticated={isAuthenticated}
                    redirectToReferrer={redirectToReferrer}
                    onLogin={this.onLogin}
                  />
                )}
              />
              <Route exact path="/register" component={Register} />
              <PrivateRoute
                exact
                path="/ipc"
                user={user}
                component={IPCVierwer}
                isAuthenticated={isAuthenticated}
              />
              <Route exact path="/about" component={IPCVierwer} />
            </React.Fragment>
          </BrowserRouter>
        </div>
      </SnackbarProvider>
    );
  }
}

export default App;
