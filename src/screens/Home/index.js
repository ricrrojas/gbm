import React from "react";
import { Card, CardContent, CardHeader } from "@material-ui/core";

import SwipeableViews from "react-swipeable-views";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import api from "../../api";
import Login from "../Login";
import Register from "../Registro";
import GoogleSignIn from "../../components/GoogleSignIn";

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  swipeableViews: {
    margin: "10px 0"
  }
};

class Home extends React.Component {
  state = {
    value: 0
  };

  componentDidMount() {
    const data = localStorage.getItem("user");
    if (data) {
      const {
        onLogin,
        history,
        location: { state: { from = { pathname: "/ipc" } } = {} }
      } = this.props;
      const user = JSON.parse(data);
      const { token } = user;
      api.setAuthorization(token);
      onLogin({ user }, () => history.replace(from));
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { value } = this.state;
    const { isAuthenticated } = this.props;
    const {
      onLogin,
      history,
      location: { state: { from = { pathname: "/ipc" } } = {} }
    } = this.props;

    return (
      <div style={styles.container}>
        {!isAuthenticated && (
          <Card>
            <CardHeader title="Login" />
            <CardContent>
              <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
              >
                <Tab label="Login" />
                <Tab label="Registro" />
              </Tabs>
              <SwipeableViews
                style={styles.swipeableViews}
                index={value}
                onChangeIndex={this.handleChangeIndex}
              >
                <Login from={from} history={history} onLogin={onLogin} />
                <Register />
              </SwipeableViews>

              <GoogleSignIn from={from} history={history} onLogin={onLogin} />
            </CardContent>
          </Card>
        )}
      </div>
    );
  }
}

export default Home;
