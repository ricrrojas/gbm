// @flow
import React from "react";
import Button from "@material-ui/core/Button";
import LinearProgress from "@material-ui/core/LinearProgress";
import moment from "moment";
import TextField from "../../components/TextInput";
import api from "../../api";
import styles from "./styles.module.css";
import { formatDatetime } from "../../utils/date";
import IPCGraph from "../../components/IPCGraph";

interface State {
  ipcs: Array<any>;
  loading: false;
}

interface Props {}

class IPCViewer extends React.Component<State, Props> {
  state = {
    data: [],
    left: "dataMin",
    right: "dataMax",
    refAreaLeft: "",
    refAreaRight: "",
    top: "dataMax+1",
    bottom: "dataMin-1",
    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true }, this._getIPC);
  }

  _refreshIPC = () => {
    this.setState({ loading: true }, this._getIPC);
  };

  _mapData = item => ({
    price: item.Precio,
    date: moment(item.Fecha)
      .toDate()
      .getTime()
  });

  _getIPC = async () => {
    let { resultObj } = await api.fetchIPC();
    if (!resultObj) {
      resultObj = [];
    }
    this.setState({
      data: resultObj.map(this._mapData),
      loading: false
    });
  };

  getAxisYDomain = (from, to, ref, offset) => {
    const { data } = this.state;
    const fromId = data.findIndex(d => d.date === from);
    const toId = data.findIndex(d => d.date === to);
    const refData = data.slice(fromId, toId);
    let [bottom, top] = [refData[0][ref], refData[0][ref]];

    refData.forEach(d => {
      if (d[ref] > top) top = d[ref];
      if (d[ref] < bottom) bottom = d[ref];
    });

    return [(bottom || 0) - offset, (top || 0) + offset];
  };

  zoom = () => {
    let { refAreaLeft, refAreaRight } = this.state;
    if (refAreaLeft === refAreaRight || refAreaRight === "") {
      this.setState(() => ({
        refAreaLeft: "",
        refAreaRight: ""
      }));
      return;
    }

    if (refAreaLeft > refAreaRight) {
      [refAreaLeft, refAreaRight] = [refAreaRight, refAreaLeft];
    }

    const [bottom, top] = this.getAxisYDomain(
      refAreaLeft,
      refAreaRight,
      "price",
      20
    );

    this.setState(() => ({
      refAreaLeft: "",
      refAreaRight: "",
      left: refAreaLeft,
      right: refAreaRight,
      from: formatDatetime(refAreaLeft),
      to: formatDatetime(refAreaRight),
      bottom,
      top
    }));
  };

  zoomOut = () => {
    this.setState(() => ({
      refAreaLeft: "",
      refAreaRight: "",
      left: "dataMin",
      right: "dataMax",
      top: "dataMax",
      bottom: "dataMin",
      to: "",
      from: ""
    }));
  };

  _onMouseDown = e => {
    if (e && e.activeLabel) this.setState({ refAreaLeft: e.activeLabel });
  };

  _onMouseMove = e => {
    const { refAreaLeft } = this.state;
    if (refAreaLeft) this.setState({ refAreaRight: e.activeLabel });
  };

  render() {
    const {
      data,
      left,
      right,
      refAreaLeft,
      refAreaRight,
      top,
      bottom,
      from,
      to,
      loading
    } = this.state;

    const graphProps = {
      left,
      right,
      refAreaLeft,
      refAreaRight,
      top,
      bottom,
      data,
      onMouseUp: this.zoom
    };
    return (
      <div className={styles.container}>
        <div className={styles.loaderContainer}>
          {loading && <LinearProgress />}
        </div>
        <div className={styles.formIPC}>
          <TextField
            value={from}
            margin="dense"
            label="Desde"
            InputProps={{
              readOnly: true
            }}
            className={styles.textDate}
          />
          <TextField
            value={to}
            margin="dense"
            label="Hasta"
            InputProps={{
              readOnly: true
            }}
          />
          <div className={styles.formBtns}>
            <Button
              variant="contained"
              color="secondary"
              onClick={this.zoomOut}
            >
              Reset Zoom
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={this._refreshIPC}
            >
              Actualizar
            </Button>
          </div>
        </div>

        <IPCGraph
          {...graphProps}
          onMouseDown={this._onMouseDown}
          onMouseMove={this._onMouseMove}
        />
      </div>
    );
  }
}

export default IPCViewer;
