import React from "react";
import { LinearProgress } from "@material-ui/core";
import withSnackbar from "material-ui-snackbar-provider/lib/withSnackbar";
import LoginForm from "../../components/LoginForm";
import api from "../../api";

class Login extends React.Component {
  state = {
    loading: false
  };

  static defaultProps = {
    onLogin: () => {}
  };

  _open = text => () => {
    const { snackbar } = this.props;
    snackbar.showMessage(text);
  };

  toogleLoading = loading => () => this.setState({ loading });

  login = values => {
    const { onLogin, history, from } = this.props;
    this.setState({ loading: true });
    api
      .login({ user: values })
      .then(user => {
        onLogin({ user }, () => history.replace(from));
      })
      .catch(this._open("Ocurrió un error!"))
      .finally(this.toogleLoading(false));
  };

  render() {
    const { loading } = this.state;
    return (
      <React.Fragment>
        {loading && <LinearProgress />}
        <LoginForm submit={this.login} />
      </React.Fragment>
    );
  }
}

export default withSnackbar()(Login);
