import React from "react";
import { LinearProgress } from "@material-ui/core";
import { withSnackbar } from "material-ui-snackbar-provider";
import RegisterForm from "../../components/RegisterForm";
import api from "../../api";

class Registro extends React.Component {
  state = {
    loading: false
  };

  _open = text => () => {
    const { snackbar } = this.props;
    snackbar.showMessage(text);
  };

  toogleLoading = loading => () => this.setState({ loading });

  registro = user => {
    this.setState({ loading: true });
    api
      .register({ user })
      .then(this._open("Usuario creado"))
      .catch(this._open("Ocurrió un error"))
      .finally(this.toogleLoading(false));
  };

  render() {
    const { loading } = this.state;
    return (
      <React.Fragment>
        {loading && <LinearProgress />}
        <RegisterForm submit={this.registro} />
      </React.Fragment>
    );
  }
}

export default withSnackbar()(Registro);
