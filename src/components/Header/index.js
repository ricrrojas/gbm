import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";

const styles = {
  root: {
    width: "100%"
  },
  toolbar: {
    alignItems: "center",
    justifyContent: "space-between"
  },
  avatar: {
    margin: 10
  },
  rightToolbar: {
    display: "flex",
    flexDirection: "row",
    padding: 10
  },
  btnLogout: {
    heigth: "4rem"
  }
};

class Header extends React.Component {
  static defaultProps = {
    isAuthenticated: false,
    logout: () => {}
  };

  render() {
    const {
      isAuthenticated,
      logout,
      user: { profile: { givenName, imageUrl } = {} } = {}
    } = this.props;
    return (
      <div style={styles.root}>
        <AppBar position="static" color="default">
          <Toolbar style={styles.toolbar}>
            <Typography variant="h6" color="inherit">
              Test
            </Typography>
            {isAuthenticated && (
              <div style={styles.rightToolbar}>
                <Avatar alt={givenName} src={imageUrl} style={styles.avatar} />
                <Button onClick={logout} style={styles.btnLogout}>
                  Cerrar Sesión
                </Button>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default Header;
