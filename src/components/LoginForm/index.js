import React from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import TextInput from "../TextInput";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required(),
  password: Yup.string().required()
});

const formikEnhancer = withFormik({
  validationSchema,
  handleSubmit: (values, { props, setSubmitting }) => {
    const { submit } = props;
    submit(values);
    setSubmitting(false);
  }
});

const LoginForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    isSubmitting,
    handleSubmit,
    isValid
  } = props;
  return (
    <form
      onSubmit={handleSubmit}
      style={{ display: "flex", flexDirection: "column" }}
    >
      <TextInput
        id="email"
        label="Email"
        placeholder="Nombre de usuario"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <TextInput
        id="password"
        label="Constraseña"
        placeholder="Constraseña"
        error={touched.password && errors.password}
        value={values.password}
        onChange={handleChange}
        onBlur={handleBlur}
        type="password"
      />
      <Button
        type="submit"
        disabled={isSubmitting || !isValid}
        fullWidth
        variant="contained"
        color="primary"
      >
        Acceder
      </Button>
    </form>
  );
};

export default formikEnhancer(LoginForm);
