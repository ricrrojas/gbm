import React from "react";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";

import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";

const TextInput = ({
  type,
  id,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => (
  <FormControl error={!!error} aria-describedby={`${id}-error`}>
    <InputLabel htmlFor={id} shrink>
      {label}
    </InputLabel>
    <Input
      id={id}
      value={value}
      onChange={onChange}
      type={type}
      error={!!error}
      {...props}
    />
    <FormHelperText id={`${id}-error`}>{error}</FormHelperText>
  </FormControl>
);

export default TextInput;
