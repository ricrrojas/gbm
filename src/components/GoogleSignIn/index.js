import React from "react";
import { GoogleLogin } from "react-google-login";
import api from "../../api";

const config = {
  clientId: process.env.REACT_APP_GOOGLE_ID
};

class GoogleSignIn extends React.Component {
  googleResponse = response => {
    const { onLogin, history, from } = this.props;
    console.log(response);

    const { profileObj: profile, accessToken } = response;
    api.authGoogle(accessToken).then(user => {
      onLogin({ user: { ...user, profile } }, () => history.replace(from));
    });
  };

  render() {
    return (
      <GoogleLogin
        clientId={config.clientId}
        buttonText="Acceder con Google"
        onSuccess={this.googleResponse}
        onFailure={this.googleResponse}
      />
    );
  }
}
export default GoogleSignIn;
