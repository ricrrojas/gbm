import React from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import TextInput from "../TextInput";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email()
    .required(),
  password: Yup.string().required(),
  confirm: Yup.string()
    .oneOf([Yup.ref("password"), null])
    .required()
});

const formikEnhancer = withFormik({
  validationSchema,
  handleSubmit: (values, { props, setSubmitting }) => {
    const { submit = () => {} } = props;
    submit(values);
    setSubmitting(false);
  }
});

const RegisterForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    isSubmitting,
    handleSubmit,
    isValid
  } = props;
  return (
    <form
      onSubmit={handleSubmit}
      style={{ display: "flex", flexDirection: "column" }}
    >
      <TextInput
        id="email"
        label="Email"
        placeholder="Correo electrónico"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <TextInput
        id="password"
        label="Constraseña"
        placeholder="Constraseña"
        error={touched.password && errors.password}
        value={values.password}
        onChange={handleChange}
        onBlur={handleBlur}
        type="password"
      />
      <TextInput
        id="confirm"
        label="Confirmar Constraseña"
        placeholder="Confirmar Constraseña"
        error={touched.confirm && errors.confirm}
        value={values.confirm}
        onChange={handleChange}
        onBlur={handleBlur}
        type="password"
      />
      <Button
        type="submit"
        disabled={isSubmitting || !isValid}
        fullWidth
        variant="contained"
        color="primary"
      >
        Enviar
      </Button>
    </form>
  );
};

export default formikEnhancer(RegisterForm);
