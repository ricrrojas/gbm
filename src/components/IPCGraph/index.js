import React from "react";
import {
  LineChart,
  ReferenceArea,
  XAxis,
  Tooltip,
  Line,
  YAxis,
  CartesianGrid,
  ResponsiveContainer
} from "recharts";
import { formatTime } from "../../utils/date";
import formatCurrency from "../../utils/currency";

const styles = {
  marginGraph: { top: 5, right: 30, left: 20, bottom: 5 }
};

export default class IPCGraph extends React.Component {
  static defaultProps = {
    data: [],
    onMouseUp: () => {},
    onMouseDown: () => {},
    onMouseMove: () => {}
  };

  render() {
    const {
      data,
      left,
      right,
      refAreaLeft,
      refAreaRight,
      bottom,
      top,
      onMouseDown,
      onMouseUp,
      onMouseMove
    } = this.props;

    return (
      <ResponsiveContainer width="80%" minWidth={480} height="80%">
        <LineChart
          data={data}
          margin={styles.marginGraph}
          onMouseDown={onMouseDown}
          onMouseMove={onMouseMove}
          onMouseUp={onMouseUp}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            allowDataOverflow
            dataKey="date"
            domain={[left, right]}
            type="number"
            scale="time"
            tickFormatter={formatTime}
          />
          <YAxis
            allowDataOverflow
            domain={[bottom, top]}
            type="number"
            tickFormatter={formatCurrency}
            yAxisId="1"
            tickMargin={-5}
          />
          <Tooltip
            label="Precio"
            labelFormatter={formatTime}
            formatter={formatCurrency}
          />
          <Line
            yAxisId="1"
            dataKey="price"
            stroke="#8884d8"
            dot={false}
            type="monotone"
            animationDuration={300}
          />
          {refAreaLeft &&
            refAreaRight && (
              <ReferenceArea
                yAxisId="1"
                x1={refAreaLeft}
                x2={refAreaRight}
                strokeOpacity={0.3}
              />
            )}
        </LineChart>
      </ResponsiveContainer>
    );
  }
}
