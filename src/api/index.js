// @flow
import axios, { type AxiosInstance, AxiosRequestConfig } from "axios";

interface fetchIPCRequest {
  empresa: String;
}

interface API {
  fetchIPC(data: fetchIPCRequest): any;
}

class Api implements API {
  _axios: AxiosInstance;

  constructor(baseURL: string) {
    const config: AxiosRequestConfig = {
      baseURL,
      timeout: 30000
    };
    this._axios = axios.create(config);
  }

  setAuthorization = token => {
    this._axios.defaults.headers.common.Authorization = `Token ${token}`;
  };

  fetchIPC = ({ empresa = "IPC" } = {}): any => {
    const config: AxiosRequestConfig = {
      params: { empresa }
    };
    return this._axios.get("/ipcs/", config).then(this.__responseHandler);
  };

  authGoogle = accessToken =>
    this._axios
      .post("/users/auth/google", {
        access_token: accessToken
      })
      .then(this.__responseHandler)
      .then(this.__storeTokenFromLogin);

  __storeTokenFromLogin = ({ user }) => {
    this.setAuthorization(user.token);
    return user;
  };

  login = loginData =>
    this._axios
      .post("/users/login", loginData)
      .then(this.__responseHandler)
      .then(this.__storeTokenFromLogin);

  register = registerData =>
    this._axios
      .post("/users/register", registerData)
      .then(this.__responseHandler);

  logout = () => {
    delete this._axios.defaults.headers.common.Authorization;
  };

  __responseHandler = response => response.data;
}

const api: Api = new Api(process.env.REACT_APP_API_URL);
export default api;
